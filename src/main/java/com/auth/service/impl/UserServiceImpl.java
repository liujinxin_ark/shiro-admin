package com.auth.service.impl;

import com.auth.config.ShiroConfig;
import com.auth.entity.User;
import com.auth.entity.UserRole;
import com.auth.mapper.UserMapper;
import com.auth.mapper.UserRoleMapper;
import com.auth.service.UserService;
import com.auth.shiro.ShiroCoreParameters;
import com.auth.shiro.filter.KickoutSessionControlFilter;
import com.auth.shiro.realm.UserRealm;
import com.auth.utils.CoreConst;
import com.auth.utils.RedisClient;
import com.auth.utils.ShiroUtil;
import com.auth.utils.UUIDUtil;
import com.github.pagehelper.PageHelper;
import com.github.pagehelper.PageInfo;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.io.Serializable;
import java.util.*;

@Service
public class UserServiceImpl implements UserService {

    @Autowired
    UserMapper userMapper;

    @Autowired
    UserRoleMapper userRoleMapper;

    @Autowired
    RedisClient redisClient;

    @Autowired
    ShiroCoreParameters shiroCoreParameters;

    @Autowired
    ShiroConfig shiroConfig;

    @Override
    public User findUserSimpleInfoByUsername(String username) {
        return userMapper.findUserSimpleInfoByUsername(username, CoreConst.STATUS_VALID);
    }

    @Override
    public User findUserAllInfoInfoByUsername(String username) {
        return userMapper.findUserAllInfoInfoByUsername(username, CoreConst.STATUS_VALID);
    }

    @Override
    public void updateLastLoginTimeByUserId(String userId) {
        userMapper.updateLastLoginTime(userId);
    }

    @Override
    public PageInfo<User> findUsers(User user, int page, int limit) {
        PageHelper.startPage(page, limit);
        user.setStatus(CoreConst.STATUS_VALID);
        List<User> users = userMapper.findUsers(user);

        // 设置用户在线情况
        KickoutSessionControlFilter kickoutSessionControlFilter = shiroConfig.kickoutSessionControlFilter(shiroCoreParameters);
        for (User userInfo : users) {
            String username = userInfo.getUsername();
            LinkedList<Serializable> chcheOnlineUser = kickoutSessionControlFilter.getChcheOnlineUser(username);
            if (chcheOnlineUser == null || chcheOnlineUser.size() <= 0) {
                // 不在线
                userInfo.setOnlineStatusAndOnlineQuantity(CoreConst.NOT_ONLINE);
            } else {
                // 在线
                userInfo.setOnlineStatusAndOnlineQuantity(CoreConst.ONLINE, chcheOnlineUser.size());
            }
        }
        return new PageInfo<>(users);
    }

    @Override
    public User findUserByUserId(String userId) {
        return userMapper.findUserByUserId(userId, CoreConst.STATUS_VALID);
    }

    @Override
    public int insertUser(User user) {
        user.setUserId(UUIDUtil.getUniqueIdByUUId());
        user.setStatus(CoreConst.STATUS_VALID);
        return userMapper.insertUser(user);
    }

    @Override
    public int updateStatusBatch(List<String> userIds) {
        Map<String, Object> params = new HashMap<String, Object>(2);
        params.put("userIds", userIds);
        params.put("status", CoreConst.STATUS_INVALID);
        return userMapper.updateStatusBatch(params);
    }

    @Override
    public int updateByUserId(User user) {
        return userMapper.updateByUserId(user);
    }

    @Override
    @Transactional
    public int addAssignRole(String userId, List<String> roleIds) {
        userRoleMapper.delete(userId);
        // 包装新的用户角色关系 入库
        List<UserRole> userRoleList = new ArrayList<>();
        for (String roleId : roleIds) {
            userRoleList.add(new UserRole(userId, roleId));
        }
        return userRoleMapper.batchInstall(userRoleList);
    }

    @Override
    public User getLoginUserAllInfo() {
        String loginUserId = ShiroUtil.getLoginUserId();
        User userInfo = userMapper.findUserAndRoleByUserId(loginUserId, CoreConst.STATUS_VALID);

        // 获取用户在线情况
        KickoutSessionControlFilter kickoutSessionControlFilter = shiroConfig.kickoutSessionControlFilter(shiroCoreParameters);
        LinkedList<Serializable> chcheOnlineUser = kickoutSessionControlFilter.getChcheOnlineUser(userInfo.getUsername());
        if (chcheOnlineUser == null || chcheOnlineUser.size() <= 0) {
            // 不在线
            userInfo.setOnlineStatusAndOnlineQuantity(CoreConst.NOT_ONLINE);
        } else {
            // 在线
            userInfo.setOnlineStatusAndOnlineQuantity(CoreConst.ONLINE, chcheOnlineUser.size());
        }
        return userInfo;
    }

    @Override
    public void kickout(String userIds) {
        String[] userIdArray = userIds.split(",");
        KickoutSessionControlFilter kickoutSessionControlFilter = shiroConfig.kickoutSessionControlFilter(shiroCoreParameters);

        for (String userIdVal : userIdArray) {
            User user = userMapper.findUserByUserId(userIdVal, CoreConst.STATUS_VALID);
            // 获取在线用户的令牌，并删除认证信息
            LinkedList<Serializable> chcheOnlineUser = kickoutSessionControlFilter.getChcheOnlineUser(user.getUsername());
            for (Serializable serializable : chcheOnlineUser) {
                redisClient.del(shiroCoreParameters.getShiroRedis().getPrefixUserAuth() + serializable.toString());
            }
            // 删除用户的在线人数信息
            redisClient.del(shiroCoreParameters.getShiroRedis().getPrefixOnline() + user.getUsername());
            // 删除用户的授权信息缓存
            String classUrl = UserRealm.class.getName();
            redisClient.del(shiroCoreParameters.getShiroRedis().getPrefixOther()
                    + classUrl + ".authorizationCache:" + user.getId());
        }
    }

    @Override
    public int findUsersWhetherExistByUsernameOrUserId(String username, String userId) {
        return userMapper.findUsersWhetherExistByUsernameOrUserId(username, userId, CoreConst.STATUS_VALID);
    }

}















