package com.auth.config;

import com.github.xiaoymin.knife4j.spring.annotations.EnableKnife4j;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import springfox.documentation.builders.ApiInfoBuilder;
import springfox.documentation.builders.PathSelectors;
import springfox.documentation.builders.RequestHandlerSelectors;
import springfox.documentation.service.ApiInfo;
import springfox.documentation.spi.DocumentationType;
import springfox.documentation.spring.web.plugins.Docket;
import springfox.documentation.swagger2.annotations.EnableSwagger2;

/**
 * Swagger配置
 */
@Configuration
@EnableSwagger2
@EnableKnife4j
public class SwaggerConfig {

    /**
     * 设置基础分组(包含所有注解标注过的接口)
     */
    @Bean
    public Docket docketBase_seller() {
        return new Docket(DocumentationType.SWAGGER_2)
                .apiInfo(apiInfoSeller())
                .select()
                .apis(RequestHandlerSelectors.basePackage("com.auth.controller"))
                .paths(PathSelectors.any())     //正则匹配请求路径，并分配至当前分组，当前所有接口
                .build()
                .groupName("安全认证")           //分组名称
                .globalOperationParameters(null);
    }

    private ApiInfo apiInfoSeller() {
        return new ApiInfoBuilder()
                .title("shiro安全认证")
                .description("系统授权认证相关接口（含用户、角色、权限菜单控制等API）,接口名称包含附属标志的则说明该接口不会进行鉴权操作。")
                .termsOfServiceUrl("")
                .version("1.0")
                .build();
    }

}

