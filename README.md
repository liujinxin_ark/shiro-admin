# shiro-admin

#### 介绍
springboot整合shiro实现前后端分离架构（swagger文档协调前端开发）

#### 软件架构
架构说明
springboot + shiro + mysql + swagger

#### 使用说明
运行项目后访问 http://localhost:8080/doc.html 即可进入 swagger接口文档界面。
![在这里插入图片描述](https://img-blog.csdnimg.cn/20210304210943795.png?x-oss-process=image/watermark,type_ZmFuZ3poZW5naGVpdGk,shadow_10,text_aHR0cHM6Ly9ibG9nLmNzZG4ubmV0L3dlaXhpbl80MDUxNjkyNA==,size_16,color_FFFFFF,t_70)

1.登录操作
初始账号:admin 密码:admin 
![在这里插入图片描述](https://img-blog.csdnimg.cn/20210304211305103.png?x-oss-process=image/watermark,type_ZmFuZ3poZW5naGVpdGk,shadow_10,text_aHR0cHM6Ly9ibG9nLmNzZG4ubmV0L3dlaXhpbl80MDUxNjkyNA==,size_16,color_FFFFFF,t_70)
登录成功后需要在 文档管理 -> 全局参数设置 添加header参数。

2.获取用户可以访问的左侧菜单

使用 系统模块 -> 获取菜单列表。


# 前端请求案例

### axios请求方式
```bash
<!DOCTYPE html>
<html>
 
<head>
    <meta charset="UTF-8">
    <title>mock拦截axios请求</title>
</head>
 
<body>
 
    <!-- 本地npm安装mock和axios -->
    <!-- <script src="../node_modules//mockjs//dist//mock.js"></script> -->
    <!-- <script src="../node_modules/axios//dist/axios.js"></script> -->
 
    <!-- 官方在线引入地址（推荐使用本地） -->
    <script src="http://mockjs.com/dist/mock.js"></script>
    <script src="https://unpkg.com/axios/dist/axios.min.js"></script>
 
    <script>
        /*************** axios拦截配置 ***************************************************/
        /**
         * 请求拦截
         */
        axios.interceptors.request.use(config => {
            config.headers.common['Authorization'] = "Bearer c91afb68-7508-45d9-9193-0d004b3f42cf";
            return config;
        });
 
        /**
         * 响应拦截
         */
        // axios.interceptors.response.use(response => {
        //     return response;
        // });
        /********************************************************************************/
 
 
        /*************** mock模拟数据 ****************************************************/
        // mock数据拦截http://121.36.146.10:8084/industry请求 模拟响应数据
        // Mock.mock('http://121.36.146.10:8084/industry', {
        //     'data': [{
        //         id: 1,
        //         iId: 0,
        //         industryName: "互联网/IT/电子/通信"
        //     }, {
        //         id: 2,
        //         iId: 0,
        //         industryName: "房地产"
        //     }, {
        //         id: 3,
        //         iId: 0,
        //         industryName: "金融业"
        //     }, {
        //         id: 4,
        //         iId: 0,
        //         industryName: "建筑业"
        //     }],
        //     'msg': "ok",
        //     'status': 200
        // });
        /********************************************************************************/
 
 
        //设置全局的baseURL
        axios.defaults.baseURL = 'http://localhost:8080'
 
        axios.get('/user/list').then(res => {
            console.log(res.data)
        });
    </script>
</body>
 
</html>
```


### ajax请求方式

```bash
<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>Document</title>
</head>
<body>
    
    <script src="https://cdn.staticfile.org/jquery/1.10.2/jquery.min.js"></script>
    
    <script>
        $.ajax({
            url: 'http://localhost:8080/user/list',
            type: 'get',
            beforeSend: function(xhr) {
                xhr.setRequestHeader("Authorization","Bearer a7a9886e-b534-4d4c-929e-ff2deaee5b19");
            },
            data: {
                
            },
            success:function(data){
                console.log(data)
            }
        });
    </script>

</body>
</html>
```
